#!/bin/bash

if [[ $DEFAULT_DATABASE_DSN != "" ]]; then
  export DATABASE_URL=$DEFAULT_DATABASE_DSN
fi

echo "==> Starting server up..."
node server