# use official Wiki.js image
FROM requarks/wiki:2

# Add Tini
USER root

# noop files for non python projects and local development
RUN mkdir /app
RUN echo "#!/bin/sh" > /wiki/migrate.sh && chmod +x /wiki/migrate.sh && echo "#!/bin/sh" > /app/migrate.sh && chmod +x /app/migrate.sh
RUN echo "#!/bin/sh" > /usr/local/bin/start && chmod +x /usr/local/bin/start

COPY run-wikijs-server.sh /wiki/run-wikijs-server.sh
RUN chmod +x /wiki/run-wikijs-server.sh

USER node
WORKDIR /wiki

# use postgres as possible
# unless you have plans to use MariaDB
# configure your DB using env variables in the web dahsbkard
ENV DB_TYPE postgres
#ENV DB_SSL 1

# Not needed, since Divio can handle that, just like Heroku.
# sidenote: not tested tho
ENV HEROKU 1

# expose Wiki.js's default port
ENV PORT 3000
EXPOSE 3000

# Divio don't build images with init flag
CMD ["/wiki/run-wikijs-server.sh"]
